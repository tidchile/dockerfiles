# Telefonica I+D Chile Docker Images

## Directory structure

The standard dockerimages file structure is used:

	ROOT
	|
	+- <Image Name>
		  |
		  +- <Version>
			  	|
			  	+ Dockerfile #
			  	+ docker-compose.yml #Optional
			  	+ Makefile

## How to build

Enter the directory containing the desired image, then run `make`

If the image doesn't contain a Makefile then run the docker build command

	docker build -t registry.tidnode.cl/<image_name>:<tag>

## Writing makefiles

There is a makefile with basic targets called `defvars.mk`, to use it you have
to include it in the Makefile and define some variables

	include ../../defvars.mk

	#Name of the docker images
	NAME=your-docker-image-name
	
	#Space separated list of tags
	TAGS=0.0.1 latest

## Make targets

The basic makefile targets available are

- *Dockerfile* _(default)_ : Builds the image in the current dir
- *push*: Pushes the image to the registry


## Environment Variables

| Name 			| Default Value 		| Description 						|
|---------------|-----------------------------------------------------------|
| REGISTRY		| registry.tidnode.cl	| defines the registry to use 		|
	

