REGISTRY?=registry.tidnode.cl
.PHONY: Dockerfile

# Builds dockerfiles for all tags defined in the makefile's TAGS variable
Dockerfile:
	for tag in ${TAGS}; do \
		TAGFLAG="-t ${REGISTRY}/${NAME}:$$tag "; \
		docker build $$TAGFLAG  .; \
	done;

push:
	for tag in ${TAGS}; do \
		docker push ${REGISTRY}/${NAME}:$$tag; \
	done;
