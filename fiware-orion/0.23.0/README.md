# What is Orion Context Broker?

Orion is a C++ implementation of the NGSI9/10 REST API binding developed as a part of the FIWARE platform.

More info: (github repo)[https://github.com/telefonicaid/fiware-orion]

# Getting Started

## Quickstart

In order to run, the image requires MongoDB 2.2. It's recommended to use the official one from docker.hub.

	docker run --name=mongo -d mongo:2.2 mongo --smallfiles
	docker run --name=orion --link=mongo -d tidchile/fiware-orion-new
	docker log orion

## Other options

There are several environment variables that modify the service's behavior.


###ENV BROKER_PORT

Defines the port where the service is listening _(default: 1026)_

##ENV BROKER_DATABASE_HOST

Hostname/IP of the mongo instance to connect _(default: mongo)_

###ENV BROKER_DATABASE_NAME

Database name/prefix for multitenant dbs _(default: orion)_

###BROKER_EXTRA_OPS

Extra flags to pass to the contextBroker command _(default: -multiservice)_

### Other envvars

ENV BROKER_LOG_DIR=/var/log/contextBroker
ENV BROKER_PID_FILE=/var/run/contextBroker/contextBroker.pid
