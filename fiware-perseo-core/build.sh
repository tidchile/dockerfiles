#!/bin/sh

BUILD_DIR=/tmp/perseo-core

# Install dependencies
yum update -y
yum install -y git rpm-build maven jetty-runner

# Get Perseo-core
git clone https://github.com/telefonicaid/perseo-core.git $BUILD_DIR
cd $BUILD_DIR

# Build WAR
mvn package

# Install
WARFILE=$(find . -name "fiware-perseo-core-*.war")
cp $WARFILE /root/perseo-core.war

# Cleaning
cd /root
rm -rf $BUILD_DIR
yum clean all
