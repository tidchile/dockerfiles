#!/bin/sh

BUILD_DIR=/tmp/perseo-fe

# Install dependencies
yum install -y epel-release
yum install -y git rpm-build mongodb-server c++ make nodejs npm

# Get Perseo FE
git clone https://github.com/telefonicaid/perseo-fe.git $BUILD_DIR

# Build RPM
cd $BUILD_DIR/rpm
sh create-rpm.sh 0.1 1

# Install
yum localinstall -y $(find RPMS -name "*.rpm")
