/*
 * Copyright 2015 Telefonica Investigación y Desarrollo, S.A.U
 *
 * This file is part of perseo-fe
 *
 * perseo-fe is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * perseo-fe is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with perseo-fe.
 * If not, see http://www.gnu.org/licenses/.
 *
 * For those usages not covered by the GNU Affero General Public License
 * please contact with::[contacto@tid.es]
 */

'use strict';

var PERSEO_CORE_HOST = process.env['PERSEO_CORE_HOST'] || 'perseo';
var PERSEO_CORE_PORT = process.env['PERSEO_CORE_PORT'] || '8080';
var PERSEO_CORE_URL  = 'http://' + PERSEO_CORE_HOST + ':' + PERSEO_CORE_PORT;

var ORION_HOST = process.env['ORION_HOST'] || 'orion';
var ORION_PORT = process.env['ORION_PORT'] || 1026;
var ORION_URL  = 'http://' + ORION_HOST + ':' + ORION_PORT;


var config = {};

/**
 * Default log level. Can be one of: 'DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL'
 * @type {string}
 */
config.logLevel = 'DEBUG';


/**
 * Configures the exposed API.
 */
config.endpoint = {
    host: '0.0.0.0',
    port: 9090,
    rulesPath : '/rules',
    actionsPath : '/actions/do',
    noticesPath : '/notices',
    vrPath : '/m2m/vrules',
    checkPath : '/check'
};

/**
 * Is Master this one?
 */
config.isMaster = true;

/**
 * Delay (in milliseconds) for slave to execute an action
 */
config.slaveDelay = 500;


/**
 * DB Configuration.
 */
config.mongo = {
    url : 'mongodb://localhost:27017/cep'
};

/**
 * OrionDB Configuration.
 */
config.orionDb = {
    url : 'mongodb://localhost:27017/orion',
    collection : 'entities',
    prefix : 'orion',
    batchSize: 500
};


/**
 * EPL core options
 *
 * interval is the time in milliseconds between refreshing rules
 * at core. Each <<interval>> ms, the rules are sent to core.
 */
config.perseoCore = {
    rulesURL : PERSEO_CORE_URL + '/perseo-core/rules',
    noticesURL : PERSEO_CORE_URL + '/perseo-core/events',
    interval: 60e3*1
};
/**
 * NEXT EPL core options (with HA)
 */
config.nextCore = {
    rulesURL : false,
    noticesURL : false
};


/**
 * SMTP endpoint options
 */
config.smtp = {
    port : process.env['SMTP_PORT'] || 25,
    host : process.env['SMTP_HOST'] || 'localhost'
};

/**
 * SMS endpoint options
 */
config.sms = {
    URL : 'http://locahost/smsoutbound',
    API_KEY : '',
    API_SECRET: '',
    from: 'tel:22012;phone-context=+34'
};

/**
 * Orion (Context Broker) endpoint options
 */
config.orion = {
    URL : ORION_URL + '/v1/updateContext'
};

/**
 * Collections
 * @type {{}}
 */
config.collections = {
    rules : 'rules',
    executions: 'executions'
};

/**
 * Executions TTL
 *
 * Number of seconds to expire a document in 'executions'
 * @type {Number}
 *
 */
config.executionsTTL = 1 * 24 * 60 * 60;

/**
 * Constants for missing header fields for service (Fiware-servicepath) DEFAULT_SERVICE
 * and tenant (Fiware-service) DEFAULT_TENANT
 *
 * @type {{}}
 */
config.DEFAULT_SERVICE = '/';
config.DEFAULT_TENANT  = 'unknownt';

module.exports = config;
