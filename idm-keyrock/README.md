## IDM Keyrock Docker image

The [IDM KeyRock](https://github.com/ging/fi-ware-idm) is an implementation of the FIWARE Identity Manager Generic Enabler.

Find detailed information of this Generic enabler at [Fiware catalogue](http://catalogue.fiware.org/enablers/identity-management-keyrock).
