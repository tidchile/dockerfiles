# Supported tags 

-	`4.2.0`, `latest`

# What is AuthZForce

AuthZForce is the reference implementation of the Authorization PDP GE. In this regard, it provides an API to manage XACML-based access control policies and provide authorization decisions based on such policies and the context of a given access request.

[FiWare Catalogue](http://catalogue.fiware.org/enablers/authorization-pdp-authzforce)
[FiWare User and programmers Guide](https://forge.fiware.org/plugins/mediawiki/wiki/fiware/index.php/Authorization_PDP_-_AuthZForce_-_User_and_Programmers_Guide_%28R4.2.0%29)

# How to use this image

Run the image exposing the `8080` port

	docker run --name authzforce -p 8080:8080 registry.tidnoe.cl/authzforce
