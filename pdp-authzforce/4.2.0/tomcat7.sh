#!/bin/bash

# Tomcat7 run script
# Extracted from ubuntu's 14.04 /etc/init.d/tomcat7

set -a
JAVA_HOME="/usr/lib/jvm/java-7-openjdk-amd64"
source "/etc/default/tomcat7"
CATALINA_HOME="/usr/share/tomcat7"
CATALINA_BASE="/var/lib/tomcat7"
JAVA_OPTS="-Djava.awt.headless=true -Xmx128m -XX:+UseConcMarkSweepGC"
CATALINA_PID="/var/run/tomcat7.pid"
CATALINA_TMPDIR="/tmp/tomcat7-tomcat7-tmp"
LANG=""
JSSE_HOME="/usr/lib/jvm/java-7-openjdk-amd64/jre/"
cd "/var/lib/tomcat7"
"/usr/share/tomcat7/bin/catalina.sh" run
