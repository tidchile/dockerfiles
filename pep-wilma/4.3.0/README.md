# What is Wilma PEP 

The [PEP Proxy - Wilma](https://github.com/ging/fi-ware-pep-proxy) is an implementation of PEP Proxy Generic Enabler.

[GitHub Repo](https://github.com/ging/fi-ware-pep-proxy)
[Fiware catalogue](http://catalogue.fiware.org/enablers/pep-proxy-wilma).

# How to use this image

## Start an authzforce instance

TODO

## Start an IDM instance

TODO

## Start this PEP image

TODO

## Configuration

### Envvars

	| Variable | Default Value | Description |
	| -------- | ------------- | ----------- |
	| ACCOUNT_HOST | ??? | URL where the user is directed in order to log in |
	| APP_HOSTNAME | <none> | Host of the server behind this proxy |
	| APP_PORT | <none> | Port where the request are being proxied to |
	| USERNAME | pepProxy | Username to use in config |
	| PASSWORD | pepProxy | Password to use in config |
