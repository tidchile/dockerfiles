var config = {};

config.pep_port = 80;

// Set this var to undefined if you don't want the server to listen on HTTPS
/*
config.https = {
    enabled: false,
    cert_file: 'cert/cert.crt',
    key_file: 'cert/key.key',
    port: 443
};
*/

config.account_host = process.env.ACCOUNT_HOST;

config.keystone_host = process.env.KEYSTONE_HOST;
config.keystone_port = process.env.KEYSTONE_PORT;

config.app_host = process.env.APP_HOSTNAME;
config.app_port = process.env.APP_PORT;

config.username = process.env.USERNAME;
config.password = process.env.PASSWORD;

// in seconds
config.chache_time = 300;

// if enabled PEP checks permissions with AuthZForce GE.
// only compatible with oauth2 tokens engine
config.azf = {
	enabled: true,
    host: 'idm',
    port: 6017,
    path: '/authzforce/domains/698df7f-ffd4-11e4-a09d-ed06f24e1e78/pdp'
};

// list of paths that will not check authentication/authorization
// example: ['/public/*', '/static/css/']
config.public_paths = [];

// options: oauth2/keystone
config.tokens_engine = 'oauth2';

config.magic_key = 'daf26216c5434a0a80f392ed9165b3b4';

module.exports = config;
